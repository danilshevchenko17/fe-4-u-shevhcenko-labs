import {
  BACKDROP,
  BACKDROP_ACTIVE_CLASS,
} from '../constants/backdrop';

class Backdrop {
  /**
   * Initilizes Backdrop class
   * @param {HTMLElement} backdropEl - backdrop element
   * @param {Function} closeAction - function to close popup
   */
  constructor(closeAction) {
    this.el = document.querySelector(`.${BACKDROP}`);
    this.closeAction = closeAction;

    this.addListeners();
  }

  addListeners() {
    this.el.addEventListener('click', this.handleBackdropClick.bind(this));
  }

  handleBackdropClick() {
    this.close();
    this.closeAction();
  }

  close() {
    this.el.classList.remove(BACKDROP_ACTIVE_CLASS);
  }

  open() {
    this.el.classList.add(BACKDROP_ACTIVE_CLASS);
  }
}

export default Backdrop;
