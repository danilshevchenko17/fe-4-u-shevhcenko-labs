import {
  ADD_TEACHER_POPUP_CLASS,
  ADD_TEACHER_POPUP_ACTIVE_CLASS,
  ADD_TEACHER_POPUP_TRIGGER_CLASS,
  ADD_TEACHER_POPUP_CLOSE_BUTTON_CLASS,
  ADD_TEACHER_POPUP_SUBMIT_BUTTON_CLASS,
} from '../constants/add-teacher-popup';

import Popup from '../helpers/popup';

class AddTeacherPopup {
  constructor(popupEl) {
    this.popupEl = popupEl;
    this.triggers = document.querySelectorAll(`.${ADD_TEACHER_POPUP_TRIGGER_CLASS}`);

    this.closeButtonEl = this.popupEl.querySelector(`.${ADD_TEACHER_POPUP_CLOSE_BUTTON_CLASS}`);
    this.submitButtonEl = this.popupEl.querySelector(`.${ADD_TEACHER_POPUP_SUBMIT_BUTTON_CLASS}`);

    this.popup = new Popup(popupEl, {
      popupVisibleClass: ADD_TEACHER_POPUP_ACTIVE_CLASS,
      popupTriggers: this.triggers,
      closeButtonEl: this.closeButtonEl,
    });

    this.addListeners();
  }

  addListeners() {
    this.submitButtonEl.addEventListener('click', this.submitForm.bind(this));
  }

  submitForm() {
    console.log('Submit Form Data');
    this.popup.closePopup();
  }
}

export default {
  init() {
    const popupEl = document.querySelector(`.${ADD_TEACHER_POPUP_CLASS}`);

    if (popupEl) {
      // eslint-disable-next-line
      new AddTeacherPopup(popupEl);
    }
  },
};
